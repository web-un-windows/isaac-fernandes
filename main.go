package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

func main() {
	RunServer()
}


func RunServer() {
	s := http.Server{
		Addr:    "172.22.51.148:8080",
		Handler: Cebola{},
	}

	s.ListenAndServe()
}

type Cebola struct{}

func (Cebola) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	if req.Method == "GET" && req.URL.Path == "/result" {
	

		op := req.URL.Query().Get("op")
		parts := strings.Split(op, "sum")

	
		num1, _ := strconv.Atoi(strings.TrimSpace(parts[0]))
		num2, _ := strconv.Atoi(strings.TrimSpace(parts[1]))


		result := num1 + num2


		res.Write([]byte(fmt.Sprintf("A soma de %d e %d é %d", num1, num2, result)))

		return
	}

	http.Error(res, "caminho da URL ou método HTTP inválido", http.StatusNotFound)
}



